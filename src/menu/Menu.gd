extends Node2D

onready var select_sprite := $Selection/Sprite as Sprite
onready var fade := $Fade as Polygon2D

var start := true
var done := false
var done_counter := Counter.new(30)
var done_flicker: Label
onready var fade_in_tween = get_tree().create_tween()

func _ready():
	fade.modulate = Color.white
	var _t = fade_in_tween.tween_property(
		fade,
		"modulate",
		Color.transparent,
		3.0
	).set_trans(Tween.TRANS_QUART)

func _input(event: InputEvent):
	if done: return
	if fade_in_tween.is_running() and (event is InputEventKey):
		fade_in_tween.kill()
		fade.modulate = Color.transparent

	if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
		start = not start
		select_sprite.position.y = 0 if start else 8
	elif event.is_action_pressed("ui_accept") or event.is_action_pressed("ui_select"):
		done = true
		done_flicker = ($Selection/Start if start else $Selection/Quit) as Label

func _physics_process(_delta):
	if done:
		if done_counter.decrement_stop():
			var tween := get_tree().create_tween()
			var _t := tween.tween_property(fade, "modulate", Color.white, 0.1)
			if start:
				var _u := tween.tween_callback(SceneChange, "go", ["res://levels/Start.tscn"]).set_delay(0.25)
			else:
				# using call_deferred here to prevent tween from leaking
				var _u := tween.tween_callback(get_tree(), "call_deferred", ["quit", 0]).set_delay(0.25)
		done_flicker.visible = done_counter.flicker(5, 3)
