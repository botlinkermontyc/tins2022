extends Level

onready var anim := $AnimationPlayer

func _ready():
	$Intro.visible = true
	yield(get_tree().create_timer(1.0), "timeout")
	anim.play("Intro")
