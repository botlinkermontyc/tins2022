extends Node2D
class_name Level

export var level_name := ""

onready var player := $Player as Player
onready var fx := $FX as Node2D
onready var camera := $LevelCamera as LevelCamera

func _ready():
	Util.aok(player.connect("fx", self, "_on_fx"))
	Util.aok(player.connect("landed", camera, "_manual_y_scroll"))
	camera.target = player.camera_target

func _on_fx(node: Node2D):
	fx.add_child(node)
