extends KinematicBody2D
class_name Player

signal fx(instance)
signal landed

export var freeze := false

const Dust = preload("res://fx/Dust.tscn")

onready var sprite := $AnimatedSprite as AnimatedSprite
onready var hitbox := $CollisionShape2D as CollisionShape2D
onready var camera_target := $CameraTarget as Node2D
onready var sfx_bump := $Sfx/Bump as AudioStreamPlayer
onready var sfx_jump := $Sfx/Jump as AudioStreamPlayer
onready var sfx_walk := $Sfx/Walk as AudioStreamPlayer
onready var sfx_drop := $Sfx/Drop as AudioStreamPlayer

const SPEED = 90
const ACCEL = 20
const GRAVITY = 10
const GRAVITY_SHJ = 11
const FALL_MAX = 216
const WATER_DRAG = 2.0
const JUMP = 22 * GRAVITY
const JUMP_SHJ_MULTIPLIER = 1.15
const JUMP_BUFFER =  8 # frames
const COYOTE_TIME = 15 # frames
const COYOTE_FALL =  2 # frames (before beginning to fall)
const DI_THRESHOLD = 0.25
const STUN_HITS_MAX = 2

var d := Vector2(0, 0)
var jumping := 0
var coyote := 0
var stun_hits := 0
var stun_timer := 0
var flash_timer := 0
var water := false
var attacking := 0

var iframes: int = 0

var pos_prev := PoolVector2Array()
var flip_h_prev := 0  # bitfield

func _ready():
	# HACK: there's currently what appears to be a weird bug with is_on_floor()
	# that means the player can get stuck falling if they very precisely *drop*
	# onto a diagonal edge. this seems to prevent that from happening.
#	position.x += 0.001

	pos_prev.resize(4)
	for i in range(4):
		pos_prev[i] = position

func _input(event: InputEvent):
	if freeze:
		return
	elif event.is_action_pressed("ui_select"):
		jump_start()


func jump_start():
	jumping = JUMP_BUFFER

func mkdust():
	var dust := Dust.instance() as Node2D
	dust.position = position + Vector2(3, 8)
#	if sprite.flip_h:
#		dust.scale.x = -1
#		dust.position.x += 4
	emit_signal("fx", dust)

func mkjump():
	mkdust()

# returns true if we should (aesthetically) show a hit
func _damage(dx: float) -> bool:
	if iframes or is_queued_for_deletion():
		return false

	stun_hits += 1
	if stun_hits > STUN_HITS_MAX:
		return true

	stun_timer = 0
	sprite.animation = "stun"

	var knockback := SPEED * 2
	if dx <= -DI_THRESHOLD:
		knockback *= -1
	elif dx >= DI_THRESHOLD:
		pass
	else:
		# di here
		var diff := dx - (d.x / 60.0)
		if diff < 0.0:
			knockback *= -1
		elif diff > 0.0:
			pass
		elif not sprite.flip_h:
			knockback *= -1

	d.x = knockback
	d.y = -JUMP

	flash_timer = 1

	return true

func _stun_cancel():
	stun_hits = 0
	visible = true

func gravity() -> int:
	# TODO high jump
	return GRAVITY

func _physics_process(_delta):
	if flash_timer:
		flash_timer -= 1
		var flash: bool
		if flash_timer >= 0:
			flash = (flash_timer != 0)
		else:
			if flash_timer == -6:
				flash_timer = -1
			flash = (flash_timer >= -2)

		(sprite.material as ShaderMaterial).set_shader_param("flash", flash)

	if iframes:
		iframes -= 1

	if freeze:
		return

	if stun_hits:
		var d_water_drag := d
		d.x *= 0.97
		d.y += GRAVITY
		if water:
			d = (d + d_water_drag) / 2

		var snap := Vector2(0, -1 if d.y < 0 else 1)
		d = move_and_slide_with_snap(
			d,
			snap,
			Vector2(0, -1)
		)

		if is_on_wall():
			d.x = 0

		stun_timer += 1
		visible = (
			((stun_timer % 4) > 1)
			if (stun_hits <= STUN_HITS_MAX)
			else ((stun_timer % 8) > 1)
		)

		if (d.y >= 0) and is_on_floor():
			_stun_cancel()
			attacking = 1 # allow player to hit enemies upon landing
			sfx_bump.play()

		return

	var left := Input.is_action_pressed("ui_left")
	var right := Input.is_action_pressed("ui_right")

	var d_water_drag := d

	var speed_max := SPEED / (WATER_DRAG if water else 1.0)
	if left:
		d.x -= ACCEL / (WATER_DRAG if water else 1.0)
		if d.x < -speed_max:
			d.x = -speed_max
	elif right:
		d.x += ACCEL / (WATER_DRAG if water else 1.0)
		if d.x > speed_max:
			d.x = speed_max
	else:
		if abs(d.x) <= ACCEL:
			d.x = 0
		elif d.x < 0:
			d.x += ACCEL
		elif d.x > 0:
			d.x -= ACCEL

	if left:
		sprite.flip_h = true
		sprite.offset.x = 2
	if right:
		sprite.flip_h = false
		sprite.offset.x = 0

	if (is_on_floor() or coyote) and jumping:
		d.y = -JUMP
		jumping = 0
		coyote = 0
		sfx_jump.play()
		mkjump()
	else:
		if (coyote < COYOTE_TIME - COYOTE_FALL) or (d.x == 0):
			d.y += gravity()
			d.y = min(
				d.y,
				FALL_MAX
				* ((1.0 / WATER_DRAG) if water else 1.0)
			)
		if d.y < 0 and not jump_held():
			d.y *= 0.5
		elif d.y > 0:
			sfx_jump.stop()
		if (jumping > 0) and not jump_held():
			jumping -= 1
		if coyote > 0:
			coyote -= 1

	if water:
		d = (d + d_water_drag) / 2

	var was_airborne := not is_on_floor()
	var snap := Vector2(0, -1 if d.y < 0 else 1)

	# 'attacking' is set before the player moves, so that even if they land on a
	# flower so fast that they hit the floor, they win. in other words, we want
	# to know whether they *were* falling when they last moved.
	# additionally, we add a 'grace frame'
	if d.y > gravity():
		attacking = 2
	elif attacking:
		attacking -= 1

	pos_prev.remove(0)
	pos_prev.append(position)

	flip_h_prev >>= 1
	if sprite.flip_h:
		flip_h_prev |= 8

	d = move_and_slide_with_snap(
		d,
		snap,
		Vector2(0, -1),
		true
	)

	if is_on_floor():
		#warning-ignore:integer_division
		if was_airborne:
			emit_signal("landed")
			if coyote < (COYOTE_TIME / 2):
				mkdust()
				sfx_drop.play()
				sfx_jump.stop()
		coyote = COYOTE_TIME

	if is_on_ceiling() and not sfx_bump.playing:
		sfx_bump.play()

	var sfx_walk_stop := true

	if d.y < 0:
		sprite.animation = "jump"
	elif not is_on_floor():
		sprite.animation = "fall"
	elif abs(d.x) > abs(get_floor_velocity().x):
		sprite.animation = "walk"
		sfx_walk_stop = false
	else:
		sprite.animation = "idle"

	if sfx_walk_stop:
		sfx_walk.stop()
	elif not sfx_walk.playing:
		sfx_walk.play()

func jump_held() -> bool:
	return Input.is_action_pressed("ui_select")


func _enemy_bounce():
	var y_mul := 2.0
	d.y = min(d.y * -y_mul, JUMP * -0.5)

	_physics_process(0)
	sprite.animation = "fall"

func flash():
	flash_timer = 3

func flash_toggle(on: bool):
	if on:
		flash_timer = -1
	else:
		flash_timer = 0
		(sprite.material as ShaderMaterial).set_shader_param("flash", false)

func _water_entered():
	water = true
	d *= 0.5
func _water_exited():
	water = false

func camera_position() -> Vector2:
	return (global_position + Vector2(3, 4)).floor()
