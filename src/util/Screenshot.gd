extends Node

# take screenshots in development on linux. setup:
# - add this as an autoload singleton
# - add a "screenshot" input action (F11 usually)
# - adjust scale as necessary

const SCALE_DOWN = 1.0

var prefix: String

func _ready():
	if (not OS.is_debug_build()) or (not OS.has_feature("X11")):
		queue_free()

	prefix = (
		ProjectSettings \
		.get_setting("application/config/name") \
		.replace(" ", "-") \
		.to_lower()
	)

func _input(event: InputEvent):
	if not event.is_action_pressed("screenshot"):
		return

	var image := get_viewport().get_texture().get_data()
	image.flip_y()
	if SCALE_DOWN != 1.0:
		image.resize(
			int(image.get_width() / SCALE_DOWN),
			int(image.get_height() / SCALE_DOWN),
			Image.INTERPOLATE_BILINEAR
		)

	var basename := prefix + "-" + String(OS.get_unix_time()) + ".png"
	var path := OS.get_environment("HOME") + "/" + basename

	var k := OK == image.save_png(path)
	assert(k)
