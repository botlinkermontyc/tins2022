extends Node

signal go

func go(path: String):
	var scene := load(path) as PackedScene
	emit_signal("go", scene)
