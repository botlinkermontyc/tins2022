extends Node

func _ready():
	if not OS.is_debug_build():
		queue_free()

func _input(event: InputEvent):
	if event.is_action_pressed("dev_quit"):
		print("[dev quit]")
		get_tree().quit(0)
