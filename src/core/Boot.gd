extends Node2D
class_name Boot

const Intro = preload("res://intro/Intro.tscn")

onready var viewport := $Viewport as Viewport
onready var sprite := $Sprite as Sprite

func _ready():
	viewport.size = get_tree().root.size
	Util.aok(SceneChange.connect("go", self, "_on_scene_change", [], CONNECT_DEFERRED))
	if OS.is_debug_build() and DevPreview.preview_scene:
		print("[dev preview] " + DevPreview.preview_scene.filename)
		viewport.add_child(DevPreview.preview_scene)
	else:
		_on_scene_change(Intro)

func _input(event: InputEvent):
	viewport.input(event)

func _on_scene_change(scene: PackedScene):
	for nchild in viewport.get_children():
		var child := nchild as Node
		child.queue_free()
	viewport.add_child(scene.instance())
