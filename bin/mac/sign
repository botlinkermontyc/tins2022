#!/bin/bash
set -eo pipefail

cd "${0%/*}/../.."

if [ -z "$1" ]; then
  echo "usage: $0 BUILD"
  echo
  echo "       signs and notarises macos builds. the macos ssh host specified in"
  echo "       bin/mac/.host ($(cat bin/mac/.host)) must be up for this to work."
  echo
  echo "       BUILD must be the version designator on the unsigned macos .zip, eg."
  echo "       '0.8.2' for 'TINS2022-0.8.2-mac-universal.zip'"
  exit 1
fi

if [ -f .env ]; then
  source .env
fi
if [ -z "$KEYCHAIN_PASS" ]; then
  read -s -p "$0: input keychain password: " KEYCHAIN_PASS
  echo
fi


T22_BUILD="$1"

DIST_UNSIGNED='dist/unsigned'
ZIP_FILENAME="TINS2022-$T22_BUILD-mac-universal.zip"

if ! [ -d "$DIST_UNSIGNED" ]; then
  echo "$0: app not built yet - run bin/export"
  exit 1
fi

function log {
  echo '---' $@
}

RM_UNSIGNED=1

for T22_LAUNCHER in itch; do
  echo "=== $T22_LAUNCHER"

  ZIP_UNSIGNED="$DIST_UNSIGNED/$T22_LAUNCHER/$ZIP_FILENAME"
  ZIP_SIGNED="dist/$T22_LAUNCHER/$ZIP_FILENAME"

  if ! [ -f "$ZIP_UNSIGNED" ]; then
    echo "WARNING: missing $ZIP_UNSIGNED"
    RM_UNSIGNED=""
    continue
  fi

  log 'prepare workspace (~/tins2022-build)'
  echo 'rm -rf tins2022-build && mkdir -p tins2022-build' | bin/mac/ssh bash

  log 'transfer build to mac'
  bin/mac/scp-up "$ZIP_UNSIGNED" tins2022-build/unsigned.zip

  log 'transfer signing script to mac'
  cat bin/mac/.sign-payload | bin/mac/ssh "cat > tins2022-build/sign"

  log 'run signing script'
  bin/mac/ssh bash -i tins2022-build/sign "$T22_BUILD" "$KEYCHAIN_PASS"

  log 'transfer build back to workstation'
  bin/mac/scp-down tins2022-build/signed.zip "$ZIP_SIGNED"

done

if [ -n "$RM_UNSIGNED" ]; then
  rm -rf "$DIST_UNSIGNED"
fi
