# TINS 2022

This repo currently doesn't have anything in it but prepared stuff for the jam (as per the rules). I'll tag the final commit before the competition starts so it's clear where the 'work' started.

I'm using a modified version of the Feral Flowers build system, plus a few bits and pieces of GDScript that'll probably be useful (eg. persistent fullscreen toggling).

## Development

Develop on Linux. You'll need Docker (+ Compose) installed.

cd to the root of the repo, then:

```sh
bin/editor        # start godot editor
bin/run           # just run the game in debug mode
```

## Build

To run a build (for release or debug respectively):

```sh
bin/export/full
bin/export/debug
```

`dist/` will contain the built exports for Windows, Linux & macOS.

### Release procedure

* Bump the version in `src/core/Version.gd`
* Run `bin/export/full`
* Check the binary's alright with `bin/export-run`
* If all is well, tag and commit:
  ```sh
  VERSION=vX.Y.Z
  git add .
  git commit -m "$VERSION"
  git tag -a "$VERSION"
  git push
  git push --tags
  ```
* Run macOS codesigning - nb. this won't be documented here because it's a bespoke (and LAN-dependent) setup
* [TINS uploads are here](https://tins.amarillion.org/2022/upload/)
